/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "sense/apds9960.h"

void apds9960_measure(void *input)
{
    // dummy();
    shared_struct *ina226_input = (shared_struct *)input;
    float *IRD = &(ina226_input->ird);
    xTaskHandle *display_tH = ina226_input->display_tHandle;
    xTaskHandle *comms_th = ina226_input->comms_tHandle;
    xTaskHandle *ina226_tH = ina226_input->ina226_tHandle;
    float again_arr[] = {16.0, 4.0, 1.0, 0.25};
    uint32_t notify = 0;
    
    const TickType_t frq_period = (APDS9960_SAMPLING_PERIOD / (portTICK_PERIOD_MS * m(s)));
    TickType_t cur_tick = xTaskGetTickCount();
    while (1)
    {
        xTaskNotifyWait(0, 0, &notify, (2*(INA226_SAMPLING_PERIOD))/portTICK_PERIOD_MS);
        if (notify == __INA226_TO_APDS9960_I2C_FOR_200MS)
        {
            //Control the timing of iterations
            vTaskDelayUntil(&cur_tick, frq_period);

            uint16_t cd = read_APDS9960(APDS9960_STATUS_REG, false);

            //Well within the limits ?
            if ((cd & 0x080) != 0x080)
            {
                if (alert_received())
                {
                    uint8_t again_prev = again;
                    cd = read_APDS9960(APDS9960_CDATAL_REG, true);

                    //Calculate to irrd
                    float ird = ((float)cd * (float)again_arr[again_prev]);
                    *IRD = (float)ird / ((float)APDS9960_COUNT_TO_IRRD); //uW/cm2
                    // ESP_LOGE(APDS9960_TAG, "%#x is read as clear light irradiance -> %f uW/(cm^2)", cd, *IRD);

                    xTaskNotify(*display_tH, __MEASURED__ | ___MEASURED_APDS9960, eSetValueWithoutOverwrite);
                    xTaskNotify(*comms_th, __MEASURED__ | ___MEASURED_APDS9960, eSetValueWithoutOverwrite);
                    uint8_t again_tmp = -2;
                    while (again_tmp != again)
                    {
                        again_tmp = again;
                        if ((again > APDS9960_AGAIN_1) && (cd > 0x0BFFF))
                        { //Greater than 0.75(can't be less than 4*(1/6) or risk inf. looping ) of 0xffff ?
                            decrement_again();
                            cd = cd / 4;
                        }
                        else if ((again < APDS9960_AGAIN_64) && (cd < 0x01fff))
                        { //Less than 0.5 of 0xffff/4 ? (essentiall 1/6)
                            increment_again();
                            cd = cd * 4;
                        }
                    }
                    if (again_prev != again)
                    {
                        ESP_ERROR_CHECK(update_again());
                        ESP_LOGW(APDS9960_TAG, "AGAIN updated to %d", again);
                    }
                    // //Clear interrupt
                    // ESP_ERROR_CHECK(cmd_APDS9960(APDS9960_AICLEAR_CMD));
                    ESP_ERROR_CHECK(cmd_APDS9960(APDS9960_CICLEAR_CMD));
                }
                else
                    ESP_LOGE(APDS9960_TAG, "Time's up. But no interrupt received");
            }
            else
            {
                //Clear interrupt
                ESP_ERROR_CHECK(cmd_APDS9960(APDS9960_CICLEAR_CMD));
                ESP_LOGW(APDS9960_TAG, "Oversaturated");
                //Clear the bit
                if (again > 0)
                {
                    decrement_again();
                    ESP_ERROR_CHECK(update_again());
                }
            }
            reset_APDS9960();
        }
        xTaskNotify(*ina226_tH, __APDS9960_TO_INA226_I2C_FOR_200MS, eSetValueWithoutOverwrite);
    }
}