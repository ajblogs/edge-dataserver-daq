/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "esp_netif.h"
#include "nvs.h"
#include "driver/gpio.h"


#define m(U) 0.001
#define u(U) 0.000001
/*ST7789V*/
// #define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 19
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5
#define PIN_NUM_DC   16
#define PIN_NUM_RST  23
#define PIN_NUM_BCKL 4

#define ok_c 0x00c000
#define disable_c 0xe0e0e0
#define error_c 0xf00000
#define warning_c 0xffBD33
#define watch_c 0x000E90
#define normal_c 0x000000

#define DELAY_DISPLAY_COUNTER 2 //Seconds
#define ACCUUMULATE_X_INA226_MEASUREMENTS_B4_DISPLAY 4
#define SCREEN_ROTATE_180 1
/*DHT22 module*/
#define DHT22_SAMPLING_PERIOD (2050*m(s))

/*APDS9960 */
#define APDS9960_SAMPLING_PERIOD (200*m(s)) //Limited by tasks using i2c (INA226)
#define APDS9960_ENABLE_VAL 0b00010011
#define APDS9960_ATIME_VAL 192
#define APDS9960_PERS_VAL 0
#define APDS9960_CONFIG1_VAL 0b01100000
#define APDS9960_CONTROL_VAL 0b00000010
#define APDS9960_COUNT_TO_IRRD 23.6 //(uW/cm2) at x16 gain, for white led lambda = 560mm

// #define APDS9960_ENABLE_VAL 0b00000011

/*Setup I2C interface for INA226 and APDS-9960*/
#define I2C_MASTER_PORT 0        /*!< I2C port number for master dev */
#define I2C_MASTER_SDA_IO 21            /*!< gpio number for I2C master data  */
#define I2C_MASTER_SCL_IO 22            /*!< gpio number for I2C master clock */
#define I2C_MASTER_FREQ_HZ 400000      /*!< I2C master clock frequency */

/*INA226 module*/
/*Constant values used for calculation*/
#define INA226_ADC_RESOLUTION 16
// Its a constant value and should not be changed
#define INA226_BUS_VOLTAGE_LSB (1.25*m(V))
// Its a constant value and should not be changed
#define INA226_SHUNT_VOLTAGE_LSB (2.5*u(V))
//Limitation of INA219 module
#define INA226_MAX_SENSE_CURRENT (819*m(A)) // +/-
/*!<INA129 sensor's shunt resistor value in ohms*/
#define INA226_SHUNT_RESISTOR_VAL 0.1           

/*Configuration value in accordance with INA226 datasheet @ 
https://www.ti.com/lit/ds/symlink/ina226.pdf
<(BIT-NAME)_(POR-VAL)_(BIT-NUM)>

RST_0_15, _1_14, _0_13, _0_12,
AVG2_0_11, AVG1_0_10, AVG0_0_9,
VBUSCT2_1_8, VBUSCT1_0_7, VBUSCT0_0_6,
VSHCT2_1_5, VSHCT1_0_4, VSHCT0_0_3,
MODE3_1_2, MODE2_1_1, MODE1_1_0 */
#define INA226_CONFIGURATION_VAL 0b0100001111111011
// #define INA226_TRIGGER_SHUNT_VOLTAGE ((INA226_CONFIGURATION_VAL & 0xfc)|0b001)
// #define INA226_TRIGGER_BUS_VOLTAG    ((INA226_CONFIGURATION_VAL & 0xfc)|0b010)

// Configured to send signal through alert pin when the conversion is completed
#define INA226_MASK_ENABLE_VAL 0b0000010000000000

// Max current to be sensed in this application
#define MAX_CURRENT_TO_SENSE (300*m(A))
// Max voltage to be sensed in this application
#define MAX_VOLTAGE_TO_SENSE (8200*m(V))
// // Won't work as the preprocessor can't handle floats
// #if MAX_CURRENT_TO_SENSE > INA226_MAX_SENSE_CURRENT
//     #error "The requested max current to be measured is above the maximum possible!"
// #endif

#define INA226_ERR_FACTOR 1 //0.962

// Circuit measurement
#define INA226_SAMPLING_PERIOD (200*m(s)) //Limited by tasks using i2c (APDS9960)
#define send_period 20.0
#define Mcnt_THRESHOLD ((send_period/(INA226_SAMPLING_PERIOD))+1)//+ 1 is for the data to be present for tail to be pointed to

// #define display_period 1000*m(s)
/*Switch*/
/*IO for measurements*/
#define SWITCH_IO 17
#define INA226_ALERT_SENSE_IO 15
#define ONE_WIRE_DATA_IO 13
#define APDS9960_ALERT_SENSE_IO 36

typedef struct
{
    float ccc;  //Closed circuit current
    float ccsv; //Closed circuit shunt voltage
    float ocv;  //Open circuit voltage
    float temperature;
    float RH; 
    float ird;
    uint16_t unsent_data; //Number of measurements waiting to be sent
    uint64_t sn; //Number of data measured so far.
    esp_ip4_addr_t ip;
    xTaskHandle *ina226_tHandle;
    xTaskHandle *apds9960_tHandle;
    xTaskHandle *display_tHandle;
    xTaskHandle *comms_tHandle;
    xTaskHandle *dht22_tHandle;
}
shared_struct;

//Coms Wifi
// #define DIAGNOSTIC_TEST_MESSAGE "#D#I#A#G#M#;"
#define COMMS_DATA_SERVER_IP_PORT "1531"
// #define COMMS_DATA_SERVER_URL "http://192.168.1.11:"
#define COMMS_DATA_SERVER_URL <define your server url here>
#define WIFI_SEND_RETRY_WAIT_PERIOD (10000/portTICK_PERIOD_MS)
#define WIFI_RETRY_DELAY_AFTER_MAXTRIES ((60*1000)/portTICK_PERIOD_MS)
#define NO_OF_MEASUREMENT_TO_WAIT_FOR_DISPLAY 5 // Timing the display based on measurement frequency - not elegant
#define BUFFER_SAMPLES (60*10)
// #define WIFI_SSID "Idontknow"//<ENTER your WiFi SSID>
#define WIFI_SSID <ENTER your WiFi SSID>
// #define WIFI_PSWD "Idontknow"//<ENTER your WiFi password>
#define WIFI_PSWD <ENTER your WiFi password>
//Reset restart counter settings on pressing the button on pin 0
#define RESET_COUNTER_PIN 0

//System time
#define SYSTEM_TIME_UPDATE_INTERVAL (1000*m(S))
#define COMMS_COUNT_UPDATE_INTERVAL (200*m(S))

//Message definition
#define __MESSAGE_SUPER_CLASS__ 0xf0000000
#define __MESSAGE_CLASS__ 0xff000000
#define ___MESSAGE_SUBCLASS___ 0xff0000
#define ____MESSAGE_IS____ 0x0ffff

//Inter task notification 
#define __MEASURED__ 0x21000000
    // of 24, 8 HSbits for type of measurement  and 16 remaining for the count
    #define ___MEASURED_INA226   0x210000
    #define ___MEASURED_DHT22    0x220000
    #define ___MEASURED_APDS9960 0x240000

#define __WIFI_CONNECTED 0x33000000
// #define __WIFI_DATA_SENT 0x31000000
#define __WIFI_CONNECTED_DATA_NOT_SENT 0x34000000
#define __WIFI_DISCONNECTED 0x36000000
#define __WIFI_DISCONNECTED_FOR_LONG 0x39000000
#define __WIFI_MESSAGE__ 0x30000000

#define __RESET_COUNTERS 0x46000000
#define __COUNTER_UPDATE 0x48000000
    // of 24, 8 HSbits for type of counter and 16 remaining for the count
    #define ___RESTART_COUNT_ID 0x200000
    #define ___SERVER_SUCCESSFULLY_SENT_COUNT_ID 0x300000
    #define ___WIFI_CONNECT_COUNT_ID 0x400000
    #define ___SERVER_DOWN_COUNT_ID 0x600000
    #define ___BUFFER_LOSS_COUNT_ID 0x800000

#define __UPDATE_SYSTEM_TIME 0x51000000
#define __COMS_UPDATE_COUNTER 0x53000000
// #define __UPDATE_MEASUREMENT_ASSOCIATED_DATA 0x53000000

//To pass the I2C resource from one task to another
#define __INA226_TO_APDS9960_I2C_FOR_200MS 0xA1000000
#define __APDS9960_TO_INA226_I2C_FOR_200MS 0xA3000000
