#include <Fonts/Font16.c>

#define nr_chrs_f16 96
#define chr_hgt_f16 16
#define baseline_f16 13
#define data_size_f16 8
#define firstchr_f16 32
#define max_width_f16 12

extern const unsigned char widtbl_f16[97];
extern const unsigned char* const chrtbl_f16[97];
