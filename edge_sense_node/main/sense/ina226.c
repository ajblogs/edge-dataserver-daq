/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sense/ina226.h"
static const char *INA226_TAG = "INA226";
// #include "sense/i2c_common.h"
#include "sense/ina226_app.h"
static uint16_t config_ina226_val = 0, config_ina226_sv_trigger_val = 0, config_ina226_bv_trigger_val = 0;
static float current_lsb = 0, power_lsb = 0;


esp_err_t write_INA226(uint8_t reg_addr, uint16_t *data)
{
    //Address must point to one of the INA226's accessible register
    assert(reg_addr < 0x08);
    uint8_t data_h = (*data >> 8);
    uint8_t data_l = (*data & 0xFF);
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    //The write procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf,  Figure 15
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA226_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_h, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_l, ACK_CHECK_EN);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(INA226_TAG, "Error writing register");
    }
    return ret;
}

uint16_t read_INA226(uint8_t reg_addr)
{
    //Address must point to one of the INA226's accessible register
    assert(reg_addr < 0x08 || reg_addr == 0xFE || reg_addr == 0xFF);
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    uint8_t data_l = 0xff;
    uint8_t data_h = 0xff;

    //Address pointer set procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/INA226_TAG.pdf, Figure 18
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA226_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(INA226_TAG, "Error setting the register pointer");
        return -1;
    }

    //The read procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/INA226_TAG.pdf, Figure 16
    // vTaskDelay(1/portTICK_PERIOD_MS);
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA226_SENSOR_ADDR << 1) | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);

    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(INA226_TAG, "Error reading register");
        return -1;
    }
    i2c_cmd_link_delete(cmd);
    return ((data_h << 8) | data_l);
}

inline void reset_INA226()
{
    uint16_t reset = 0x8000;
    ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &reset));
}

bool INA226_connected()
{
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA226_SENSOR_ADDR << 1) | WRITE_BIT,
                          ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd,
                               1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        ESP_LOGE(INA226_TAG, "INA226 device not found!");
        return false;
    }
    return true;
}

//Configure the sensor settings.
void configure_override_INA226(uint16_t config_data)
{
    config_ina226_val = config_data;
    config_ina226_sv_trigger_val = ((config_data & 0xfc)|0b001);
    config_ina226_bv_trigger_val = ((config_data & 0xfc)|0b010);
    vTaskDelay(1 / portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_data));
}

//Configure the sensor settings.
void calibration_override_INA226(uint16_t cal_constant)
{
    ESP_ERROR_CHECK(write_INA226(INA226_CALIB_REG_ADDR, &cal_constant));
}

void mask_enable_override_INA226(uint16_t val)
{
    ESP_ERROR_CHECK(write_INA226(INA226_MASK_ENABLE_REG_ADDR, &val));
}

//Trigger sv - shunt voltage and bv -  bus voltage conversions in one-shot mode
void single_shot_trigger_sv_conversion()
{
    ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_sv_trigger_val));
    // ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_val));
    // ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_bv_trigger_val));
}

void single_shot_trigger_bv_conversion()
{
    ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_bv_trigger_val));
    // ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_val));
    // ESP_ERROR_CHECK(write_INA226(INA226_CONFIG_REG_ADDR, &config_ina226_sv_trigger_val));

}

// The value is set after initialization
float current_LSB()
{
    return current_lsb;
}

float power_LSB()
{
    return power_lsb;
}
static inline void initiate_alert_sense()
{
    gpio_config_t switch_config = {.intr_type = GPIO_INTR_DISABLE,
                                   .mode = GPIO_MODE_INPUT,
                                   .pin_bit_mask = (1ULL << INA226_ALERT_SENSE_IO),
                                   .pull_down_en = 0,
                                   .pull_up_en = 1};
    ESP_ERROR_CHECK(gpio_config(&switch_config));
}

static bool alert_received()
{
    if (gpio_get_level(INA226_ALERT_SENSE_IO) == 0)
        return true;
    return false;
}

void initialize_INA226()
{
    initiate_alert_sense();

    if (!INA226_connected())
    {
        ESP_LOGE(INA226_TAG, "Not found INA226");
        exit(1);
    }
    else
        ESP_LOGW(INA226_TAG, "Connected to INA226");
    //Reset the device
    reset_INA226();

    // Check and set max ampere expected to be sensed
    assert(MAX_CURRENT_TO_SENSE > 0.0);
    if (MAX_CURRENT_TO_SENSE > INA226_MAX_SENSE_CURRENT)
    {
        ESP_LOGE(INA226_TAG, "Max allowed sense current is %f. \
                 Unable to set the requested max_current value !",
                 INA226_MAX_SENSE_CURRENT);
        while (1)
            ;
    }

    current_lsb = (float)MAX_CURRENT_TO_SENSE / 32768.0;
    power_lsb = current_lsb * 25;
    float cal_val = 0.00512 / (current_lsb * (float)INA226_SHUNT_RESISTOR_VAL);

    ESP_LOGI(INA226_TAG, "Calibration register (%#02X) set: %#04x", INA226_CALIB_REG_ADDR, (uint16_t)cal_val);
    calibration_override_INA226((uint16_t)cal_val);

    uint16_t tmp;
    //Cross check written value
    tmp = read_INA226(INA226_CALIB_REG_ADDR);
    if (tmp == (uint16_t)cal_val)
    {
        ESP_LOGI(INA226_TAG,
                 "Readback check calibration value: OK");
    }
    else
    {
        ESP_LOGE(INA226_TAG,
                 "Readback check calibration value: FAIL\nReceived: %#04x", tmp);
        exit(1);
    }

    ESP_LOGI(INA226_TAG, "Mask/Enable register (%#02X) set: %#04x", INA226_MASK_ENABLE_REG_ADDR, INA226_MASK_ENABLE_VAL);
    mask_enable_override_INA226(INA226_MASK_ENABLE_VAL);

    tmp = read_INA226(INA226_MASK_ENABLE_REG_ADDR);
    //Cross check written value. Conversion ready is
    if (tmp == (INA226_MASK_ENABLE_VAL | 1 << 3) || tmp == INA226_MASK_ENABLE_VAL)
    {
        ESP_LOGI(INA226_TAG,
                 "Readback check mask/enable value: OK");
    }
    else
    {
        ESP_LOGE(INA226_TAG,
                 "Readback check mask/enable value: FAIL\nReceived: %#04x", tmp);
        exit(1);
    }

    //Configuration is set at last to trigger the conversion at last
    ESP_LOGI(INA226_TAG, "Configuration register (%#02X) set: %#04x", INA226_CONFIG_REG_ADDR, INA226_CONFIGURATION_VAL);
    configure_override_INA226(INA226_CONFIGURATION_VAL);

    //Cross check written value
    tmp = read_INA226(INA226_CONFIG_REG_ADDR);
    if (tmp == INA226_CONFIGURATION_VAL)
    {
        ESP_LOGI(INA226_TAG,
                 "Readback check configuration value: OK");
    }
    else
    {
        ESP_LOGE(INA226_TAG,
                 "Readback check configuration value: FAIL\nReceived: %#04x", tmp);
        exit(1);
    }
}

esp_err_t getdata_INA226(ina226_struct *idat)
{
    uint16_t raw_volt = 0xffff, raw_shunt_volt = 0xffff, raw_I = 0xffff, raw_P = 0xffff;

    // Conversion Ready check
    if (!alert_received())
    {
        ESP_LOGE(INA226_TAG, "Alert not received!");
        return ESP_FAIL;
    }

    raw_volt = read_INA226(INA226_BUS_VOLTAGE_REG_ADDR);

    // SDA/SCL check
    if (raw_volt == 0xFFFF)
    {
        idat->overflow = false;
        idat->cnv_time_err = false;
        idat->stale = true;
        if (!INA226_connected())
        {
            idat->disconnected = true;
        }
        else
            ESP_LOGE(INA226_TAG, "Device connected. Unknown error!");
        return ESP_FAIL;
    }

    idat->disconnected = false;
    //Overflow check
    uint16_t check = read_INA226(INA226_MASK_ENABLE_REG_ADDR);
    if (check & 0b0100)
    {
        ESP_LOGE(INA226_TAG, "Overflow error: Possible short circuit detected!");
        idat->overflow = true;
        idat->cnv_time_err = false;
        idat->stale = true;
    }
    //Conversion ready
    else if (check & 0b01000)
    {
        raw_shunt_volt = read_INA226(INA226_SHUNT_VOLTAGE_REG_ADDR);
        raw_I = read_INA226(INA226_CURRENT_REG_ADDR);
        raw_P = read_INA226(INA226_POWER_REG_ADDR);
        float V = (float)(raw_volt) * (float)INA226_BUS_VOLTAGE_LSB;
        float I = (float)((int16_t)raw_I) * current_lsb;
        float P = (float)((int16_t)raw_P) * power_lsb;
        idat->stale = false;
        idat->overflow = false;
        idat->cnv_time_err = false;
        idat->raw_voltage = raw_volt;
        idat->raw_shunt_voltage = raw_shunt_volt;
        idat->V = V;
        idat->I = I;
        idat->P = P;
        float Vshunt = ((float)((int16_t)raw_shunt_volt) * (float)INA226_SHUNT_VOLTAGE_LSB);
        idat->Vshunt = Vshunt;
        float Icalc = Vshunt / (float)INA226_SHUNT_RESISTOR_VAL;
        Icalc *= INA226_ERR_FACTOR;
        idat->Icalc = Icalc;

        //For power calculation, we are using absolute value
        if (Icalc < 0)
            Icalc = -Icalc;
        float Pcalc = (Icalc)*V;
        idat->Pcalc = Pcalc;

        ESP_LOGI(INA226_TAG, "Received data\n| %-10s:%#04x,%03.10f |\n| %-10s:%#04x,%03.10f |\n| %-10s:%03.10f,%03.10f |\n",
                 "r.I,I", raw_I, I, "r.P,P", raw_P, P, "I.c,P.c", Icalc, Pcalc);
        // printf("shunt:%#04x volt:%#04x curent:%#04x power:%#04x\n",raw_shunt_volt,raw_volt,raw_current,raw_power);
        // printf("%f is the voltage while %f is the current, %f is the power\n",V,Icalc,idat->Pcalc);
        // }
        return ESP_OK;
    }
    else
    {
        ESP_LOGE(INA226_TAG, "!!!Conversion flag not set. Shouldn't be possible as the alert for conversion received!!!");
        while (1)
            ;
        idat->cnv_time_err = true;
        idat->overflow = false;
        idat->stale = true;
    }
    return ESP_FAIL;
}
