/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/i2c.h"

static const char *I2C_TAG = "I2C_COMMON";

//Function 1
static inline void i2c_interface_reset()
{
    //General technique to free the bus
    for (uint8_t i = 0; i < 50;i++)
    {
        gpio_set_level(I2C_MASTER_SCL_IO, (01 ^ gpio_get_level(I2C_MASTER_SCL_IO)));
        vTaskDelay(1 / portTICK_PERIOD_MS);
    }

    //To reset the INA226 to be in sync after asynchronous reset
    gpio_set_direction(I2C_MASTER_SCL_IO, GPIO_MODE_OUTPUT_OD);
    gpio_set_level(I2C_MASTER_SCL_IO, 0);
    //SCL pulled to low for 28-35ms will reset the INA226. Higher value just to be sure.
    vTaskDelay(50 / portTICK_PERIOD_MS);
}

i2c_port_t i2c_master_init()
{
    i2c_port_t i2c_master_port = I2C_MASTER_PORT;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    conf.clk_flags = 0,
    i2c_param_config(i2c_master_port, &conf);
    ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode,
                                       I2C_MASTER_RX_BUF_DISABLE,
                                       I2C_MASTER_TX_BUF_DISABLE, 0));
    ESP_LOGI(I2C_TAG, "I2C master initiated!");
    return i2c_master_port;
}
