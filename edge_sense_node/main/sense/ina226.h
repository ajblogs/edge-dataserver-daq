/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/i2c.h"

#define I2C_MASTER_TX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

/*INA226 Register address map*/
#define INA226_SENSOR_ADDR  0x40                /*!<INA226 sensor's address */
#define INA226_CONFIG_REG_ADDR 0x00             
#define INA226_SHUNT_VOLTAGE_REG_ADDR 0x01
#define INA226_BUS_VOLTAGE_REG_ADDR 0x02
#define INA226_POWER_REG_ADDR 0x03
#define INA226_CURRENT_REG_ADDR 0x04
#define INA226_CALIB_REG_ADDR 0x05
#define INA226_MASK_ENABLE_REG_ADDR 0x06
#define INA226_ALERT_LIMIT_REG_ADDR 0x07
#define INA226_MID_REG_ADDR 0xFE
#define INA226_DID_REG_ADDR 0xFF

typedef struct {
   uint16_t raw_current;
   uint16_t raw_voltage;
   uint16_t raw_power;
   uint16_t raw_shunt_voltage;
   float Vshunt;
   float Pcalc;
   float Icalc;
   float P;
   float I;
   float V;
   bool stale;
   bool overflow;
   bool cnv_time_err;
   bool disconnected;
} ina226_struct;

i2c_port_t i2c_master_init();

void initialize_INA226();

void reset_INA226();

bool INA226_connected();

esp_err_t write_INA226(uint8_t reg_addr, uint16_t *data);

uint16_t read_INA226(uint8_t reg_addr);

esp_err_t getdata_INA226(ina226_struct *);

float current_LSB();

float power_LSB();

void single_shot_trigger_sv_conversion();

void single_shot_trigger_bv_conversion();

void switchInitiate();

void ina226_measure(void *);