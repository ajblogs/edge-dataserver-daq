/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "actuate/comms.h"
#include "others/forever_counters.h"

static const char *COMMS_TAG = "COMMS_WIFI";

static int retry_num = 0;

static esp_ip4_addr_t ip_addr;
//Contains response to wifi related events
static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        xEventGroupClearBits(wifi_event_group, WIFI_CONNECTED_BIT);
        if (retry_num < WIFI_MAXIMUM_RETRY)
        {
            ESP_LOGI(COMMS_TAG, "retry to connect to the AP");
            retry_num++;
            esp_wifi_connect();
        }
        else
        {
            xEventGroupSetBits(wifi_event_group, WIFI_FAIL_BIT);
            vTaskDelay(WIFI_RETRY_DELAY_AFTER_MAXTRIES);
            esp_wifi_connect();
        }
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED)
    {
        increment_wifi_connect_counter();
    }
}

static void on_got_ip(void *arg, esp_event_base_t event_base,
                      int32_t event_id, void *event_data)
{
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    ip_addr = event->ip_info.ip;
    ESP_LOGI(COMMS_TAG, "got ip:" IPSTR, IP2STR(&ip_addr));
    retry_num = 0;
    xEventGroupSetBits(wifi_event_group, WIFI_CONNECTED_BIT);
    xEventGroupClearBits(wifi_event_group, WIFI_FAIL_BIT);
}

esp_netif_t *wifi_start(void)
{
    char *desc;
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_event_group = xEventGroupCreate();

    esp_netif_inherent_config_t esp_netif_config = ESP_NETIF_INHERENT_DEFAULT_WIFI_STA();
    // Prefix the interface description with the module TAG
    // Warning: the interface desc is used in tests to capture actual connection details (IP, gw, mask)
    asprintf(&desc, "%s: %s", COMMS_TAG, esp_netif_config.if_desc);
    esp_netif_config.if_desc = desc;
    esp_netif_config.route_prio = 128;
    esp_netif_t *netif = esp_netif_create_wifi(WIFI_IF_STA, &esp_netif_config);
    free(desc);
    esp_wifi_set_default_wifi_sta_handlers();

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip, NULL));

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            
            // .scan_method = EXAMPLE_WIFI_SCAN_METHOD,
            // .sort_method = EXAMPLE_WIFI_CONNECT_AP_SORT_METHOD,
            // .threshold.rssi = CONFIG_EXAMPLE_WIFI_SCAN_RSSI_THRESHOLD,
            .ssid = WIFI_SSID,
            .password = WIFI_PSWD,
            .threshold.authmode = WIFI_AUTH_WPA2_PSK,
            .pmf_cfg = {
                .capable = true,
                .required = false},
        },
    };

    ESP_LOGI(COMMS_TAG, "Connecting to %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    return netif;
}

// static void wifi_stop(void)
// {
//     esp_netif_t *wifi_netif = get_example_netif_from_desc("sta");
//     ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect));
//     ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip));

//     esp_err_t err = esp_wifi_stop();
//     if (err == ESP_ERR_WIFI_NOT_INIT) {
//         return;
//     }
//     ESP_ERROR_CHECK(err);
//     ESP_ERROR_CHECK(esp_wifi_deinit());
//     ESP_ERROR_CHECK(esp_wifi_clear_default_wifi_driver_and_handlers(wifi_netif));
//     esp_netif_destroy(wifi_netif);
//     s_example_esp_netif = NULL;
// }

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    static char *output_buffer; // Buffer to store response of http request from event handler
    static int output_len;      // Stores number of bytes read
    switch (evt->event_id)
    {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        /*
             *  Check for chunked encoding is added as the URL for chunked encoding used in this example returns binary data.
             *  However, event handler can also be used in case chunked encoding is used.
             */
        if (!esp_http_client_is_chunked_response(evt->client))
        {
            // If user_data buffer is configured, copy the response into the buffer
            if (evt->user_data)
            {
                memcpy(evt->user_data + output_len, evt->data, evt->data_len);
            }
            else
            {
                if (output_buffer == NULL)
                {
                    output_buffer = (char *)malloc(esp_http_client_get_content_length(evt->client));
                    output_len = 0;
                    if (output_buffer == NULL)
                    {
                        ESP_LOGE(COMMS_TAG, "Failed to allocate memory for output buffer");
                        return ESP_FAIL;
                    }
                }
                memcpy(output_buffer + output_len, evt->data, evt->data_len);
            }
            output_len += evt->data_len;
        }

        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(COMMS_TAG, "HTTP_EVENT_ON_FINISH");
        if (output_buffer != NULL)
        {
            // Response is accumulated in output_buffer. Uncomment the below line to print the accumulated response
            // ESP_LOG_BUFFER_HEX(COMMS_TAG, output_buffer, output_len);
            free(output_buffer);
            output_buffer = NULL;
        }
        output_len = 0;
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGI(COMMS_TAG, "HTTP_EVENT_DISCONNECTED");
        int mbedtls_err = 0;
        esp_err_t err = esp_tls_get_and_clear_last_error(evt->data, &mbedtls_err, NULL);
        if (err != 0)
        {
            if (output_buffer != NULL)
            {
                free(output_buffer);
                output_buffer = NULL;
            }
            output_len = 0;
            ESP_LOGI(COMMS_TAG, "Last esp error code: 0x%x", err);
            ESP_LOGI(COMMS_TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
        }
        break;
    }
    return ESP_OK;
}

static esp_http_client_handle_t client;
static char local_response_buffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
static esp_http_client_config_t config = {
    // .host = "httpbin.org",
    // .path = "/get",
    // .query = "esp",
    .url = COMMS_DATA_SERVER_URL COMMS_DATA_SERVER_IP_PORT,
    .event_handler = _http_event_handler,
    .user_data = local_response_buffer, // Pass address of local buffer to get response
    .disable_auto_redirect = true,
};

void send_data(void *input)
{
    const char *data = (const char *)input;
    ESP_LOGI(COMMS_TAG, "Sending data using HTTP");
    ESP_LOGI(COMMS_TAG, "\nDATA:\n%s", data);
    // char local_response_buffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
    /**
     * NOTE: All the configuration parameters for http_client must be spefied either in URL or as host and path parameters.
     * If host and path parameters are not set, query parameter will be ignored. In such cases,
     * query parameter should be specified in URL.
     *
     * If URL as well as host and path parameters are specified, values of host and path will be considered.
     */
    EventBits_t wifi_status = xEventGroupGetBits(wifi_event_group);

    if (wifi_status & WIFI_SEND_FAIL_BIT)
    {
        client = esp_http_client_init(&config);
        ESP_LOGE(COMMS_TAG, "Sending data - After fail");
        xEventGroupClearBits(wifi_event_group, WIFI_SEND_FAIL_BIT);
    }
    else if (wifi_status & WIFI_FIRST_SEND_BIT)
    {
        xEventGroupClearBits(wifi_event_group, WIFI_FIRST_SEND_BIT);
        client = esp_http_client_init(&config);
        ESP_LOGE(COMMS_TAG, "Sending data - FIRST TIME");
    }

    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_header(client, "Solar-PV-data", "newline-seperated");
    esp_http_client_set_post_field(client, data, strlen(data));
    esp_err_t err = esp_http_client_perform(client);
    if (err == ESP_OK)
    {
        ESP_LOGI(COMMS_TAG, "HTTP POST Status = %d, content_length = %d",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
        xEventGroupSetBits(wifi_event_group, WIFI_SEND_SUCCESS_BIT);
    }
    else
    {
        ESP_LOGE(COMMS_TAG, "HTTP POST request failed: %s", esp_err_to_name(err));
        xEventGroupSetBits(wifi_event_group, WIFI_SEND_FAIL_BIT);
        wifi_send_fail_tick = xTaskGetTickCount();
        esp_http_client_cleanup(client);
    }
    vTaskDelete(NULL);
}

void comms(void *input)
{
    uint64_t mcnt_sn = 0;
    uint32_t notify = 0;
    shared_struct *coms_input = (shared_struct *)input;
    xTaskHandle *display_tH = coms_input->display_tHandle;
    float *OCV = &coms_input->ocv;
    float *CCC = &coms_input->ccc;
    float *CCSV = &coms_input->ccsv;
    float *IRD = &coms_input->ird;
    float *Temp = &coms_input->temperature;
    float *RH = &coms_input->RH;
    uint16_t *unsent_data_ = &coms_input->unsent_data;
    esp_ip4_addr_t *dev_ip = &coms_input->ip;
    uint64_t *sn_ = &coms_input->sn;

    char *data[BUFFER_SAMPLES];
    for (int i = 0; i < BUFFER_SAMPLES; i++)
        data[i] = (char *)malloc(8 * 10);
    uint16_t mcnt = 0;
    uint16_t mcnt_tail = 0;
    char *data_send = (char *)malloc((Mcnt_THRESHOLD * 8 * 10) + 200);

    //Find the lenght between head and tail of mcnt
    uint16_t mcnt_valid_length()
    {
        if (mcnt > mcnt_tail)
            return (mcnt - mcnt_tail + 1);
        else if (mcnt < mcnt_tail)
            return ((BUFFER_SAMPLES - mcnt_tail) + (mcnt + 1));
        else
            ESP_LOGE(COMMS_TAG, "MCNT tail and head can never be equal and assert should fail. something wrong (except the first iteration)");
        return 0;
    }

    void inline mcnt_tail_increment()
    {
        mcnt_tail++;
        if (mcnt_tail == BUFFER_SAMPLES)
            mcnt_tail = 0;
    }

    bool previous_send_success = true;
    //Setting the send success bit to initiate first http transfer
    xEventGroupSetBits(wifi_event_group, WIFI_FIRST_SEND_BIT);
    //Server test
    sprintf(data_send,"DIAGNOSTIC_TEST_MESSAGE");
    xTaskCreate(send_data,"Server reachability test ?", 8000,(void *) data_send, 10, NULL);
    uint32_t notify_class = 0;
    bool cnt_ready = false;
    float ird = 0;
    float temp = 0;
    float rh = 0;
    float ccc = 0;
    float ocv = 0;
    float ccsv = 0;
    bool measurement_apds9960_ready = false;
    bool measurement_ina226_ready = false;
    bool measurement_dht22_ready = false;
    while (1)
    {
        xTaskNotifyWait(0, 0, &notify, portMAX_DELAY);
        notify_class = (notify & __MESSAGE_CLASS__);
        if (notify == __COMS_UPDATE_COUNTER)
        {
            mcnt_sn++;
            cnt_ready = true;
        }
        else if (notify_class == __MEASURED__)
        {
            uint32_t notify_sub_class = (notify & ___MESSAGE_SUBCLASS___);
            // ESP_LOGW(COMMS_TAG, "messge subclass %#x", notify_sub_class);
            switch (notify_sub_class)
            {

            case ___MEASURED_INA226:
                ocv = *OCV;
                ccc = *CCC;
                ccsv = *CCSV;
                measurement_ina226_ready = true;
                break;

            case ___MEASURED_DHT22:
                temp = *Temp;
                rh = *RH;
                measurement_dht22_ready = true;
                break;
            case ___MEASURED_APDS9960:
                ird = *IRD;
                measurement_apds9960_ready = true;
                break;
            default:
                ESP_LOGE(COMMS_TAG, "Shouldn't happen while decoding counter info");
                while (1)
                    ;
                break;
            }
        }
        if (cnt_ready == true)
        {
            cnt_ready = false;
            char strftime_buf[200];
            if (mcnt_sn == 1)
            {
                ESP_LOGI(COMMS_TAG, "Time stamp ...");
                time_t now = 0;
                struct tm timeinfo = {0};
                setenv("TZ", "UTC", 1);
                tzset();
                time(&now);
                localtime_r(&now, &timeinfo);
                strftime(strftime_buf, sizeof(strftime_buf), "UTC: %c", &timeinfo);
                sprintf(strftime_buf + strlen(strftime_buf), ";CNTp:%f#INA226sp:%fs - OCV(V),OCC(A),CCSV(V)#APDS9960sp:%fs - Irradiance(uW/cm2,x16,23.6cnt/(uW/cm2))#DHT22sp:%fs - Temp(C),RH(%%)", COMMS_COUNT_UPDATE_INTERVAL, INA226_SAMPLING_PERIOD, APDS9960_SAMPLING_PERIOD, DHT22_SAMPLING_PERIOD);
                // ESP_LOGE(COMMS_TAG, "%s\n", strftime_buf);
                xEventGroupSetBits(wifi_event_group, WIFI_TIME_SEND_BIT);
            }
            EventBits_t wifi_status = xEventGroupGetBits(wifi_event_group);
            ESP_LOGI(COMMS_TAG, "\n\nWifi status: %d", wifi_status);
            sprintf(data[mcnt], "%lld", mcnt_sn);
            if (measurement_ina226_ready == true)
            {
                sprintf(data[mcnt] + strlen(data[mcnt]), "#%f,%f,%f", ocv, ccc, ccsv);
                measurement_ina226_ready = false;
            }
            else
                sprintf(data[mcnt] + strlen(data[mcnt]), "#");

            if (measurement_apds9960_ready == true)
            {
                sprintf(data[mcnt] + strlen(data[mcnt]), "#%f", ird);
                measurement_apds9960_ready = false;
            }
            else
                sprintf(data[mcnt] + strlen(data[mcnt]), "#");

            if (measurement_dht22_ready == true)
            {
                sprintf(data[mcnt] + strlen(data[mcnt]), "#%f,%f", temp, rh);
                measurement_dht22_ready = false;
            }
            else
                sprintf(data[mcnt] + strlen(data[mcnt]), "#");
            sprintf(data[mcnt] + strlen(data[mcnt]), ";");
            // ESP_LOGE(COMMS_TAG, "%s - %d", data[mcnt], strlen(data[mcnt]));
            uint16_t mcnt_diff = mcnt_valid_length();

            //To update the display with coms info
            if (mcnt_sn % NO_OF_MEASUREMENT_TO_WAIT_FOR_DISPLAY == 0)
            {
                *unsent_data_ = mcnt_diff;
                *sn_ = mcnt_sn;
                static uint8_t previously_connected = false;
                if (wifi_status & WIFI_CONNECTED_BIT)
                {
                    *dev_ip = ip_addr;
                    // else
                    if (wifi_status & WIFI_SEND_SUCCESS_BIT)
                    {
                        xTaskNotify(*display_tH, __WIFI_CONNECTED, eSetValueWithoutOverwrite);
                        previous_send_success = true;
                    }
                    else if (wifi_status & WIFI_SEND_FAIL_BIT)
                    {
                        if (previous_send_success && previously_connected)
                            increment_server_down_counter();
                        previous_send_success = false;
                        xTaskNotify(*display_tH, __WIFI_CONNECTED_DATA_NOT_SENT, eSetValueWithoutOverwrite);
                    }
                    else if (previous_send_success)
                        xTaskNotify(*display_tH, __WIFI_CONNECTED, eSetValueWithoutOverwrite);
                    else
                        xTaskNotify(*display_tH, __WIFI_CONNECTED_DATA_NOT_SENT, eSetValueWithoutOverwrite);

                    previously_connected = true;
                }
                else if (wifi_status & WIFI_FAIL_BIT)
                {
                    xTaskNotify(*display_tH, __WIFI_DISCONNECTED_FOR_LONG, eSetValueWithoutOverwrite);
                    previously_connected = false;
                }
                else
                {
                    xTaskNotify(*display_tH, __WIFI_DISCONNECTED, eSetValueWithoutOverwrite);
                    previously_connected = false;
                }
            }

            if (wifi_status & WIFI_CONNECTED_BIT)
            {
                ESP_LOGI(COMMS_TAG, "mcnt_diff: %d / Mcnt_THRESHOLD: %d",
                         mcnt_diff, (int)Mcnt_THRESHOLD);

                if ((mcnt_diff >= Mcnt_THRESHOLD) && ((wifi_status & WIFI_SEND_SUCCESS_BIT) || (wifi_status & WIFI_FIRST_SEND_BIT)))
                {
                    xEventGroupClearBits(wifi_event_group, WIFI_SEND_SUCCESS_BIT);
                    if (wifi_status & WIFI_TIME_SEND_BIT)
                    {
                        xEventGroupClearBits(wifi_event_group, WIFI_TIME_SEND_BIT);
                        sprintf(data_send, "$%s;\n", strftime_buf);
                        sprintf(data_send + strlen(data_send), "%s\n", data[mcnt_tail]);
                    }
                    else
                        sprintf(data_send, "%s\n", data[mcnt_tail]);
                    for (int i = 0; i < (Mcnt_THRESHOLD - 2); i++)
                    {
                        mcnt_tail_increment();
                        sprintf(data_send + strlen(data_send), "%s\n", data[mcnt_tail]);
                    }
                    mcnt_tail_increment();
                    ESP_LOGI(COMMS_TAG, "%s - %d", data_send, strlen(data_send));
                    ESP_LOGW(COMMS_TAG, "Normal data send");
                    xTaskCreate(send_data, "Send data using HTTP", 8000,
                                (void *)data_send, 10, NULL);
                }
                else if ((wifi_status & WIFI_SEND_FAIL_BIT) && (WIFI_SEND_RETRY_WAIT_PERIOD < (xTaskGetTickCount() - wifi_send_fail_tick)))
                {
                    ESP_LOGW(COMMS_TAG, "Retry data send");
                    xTaskCreate(send_data, "Send data using HTTP", 8000,
                                (void *)data_send, 10, NULL);
                }
                else
                    ESP_LOGI(COMMS_TAG, "HTTP client processing!");
            }
            ESP_LOGI(COMMS_TAG, "MCNT = %d - %d / %d", mcnt, mcnt_tail, BUFFER_SAMPLES);

            //Chekup and update mcnt and mcnt_tail indexes.
            if (mcnt < (BUFFER_SAMPLES - 1))
                mcnt++;
            else
                mcnt = 0;

            if (mcnt == mcnt_tail)
            {
                mcnt_tail_increment();
                increment_buffer_loss_counter();
                ESP_LOGE(COMMS_TAG, "Loosing samples");
            }
            assert(mcnt_tail < BUFFER_SAMPLES);
            assert(mcnt < BUFFER_SAMPLES);
            assert(mcnt != mcnt_tail);
        }
    }
}