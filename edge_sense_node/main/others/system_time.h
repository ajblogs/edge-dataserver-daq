/*
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "esp_sntp.h"

static const char* ST_TAG = "SYS_TIME";

void system_time_initiate(){

    setenv("TZ", "UTC", 1);
    tzset();
    ESP_LOGI(ST_TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();

    int retry = 0;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET) {
        ESP_LOGW(ST_TAG, "Waiting for system time to be set... (%d)", ++retry);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        if (retry > 20) esp_restart();
    }
}

void system_time(void* inp){
    shared_struct* input = (shared_struct *)inp;
    xTaskHandle* display_th = input->display_tHandle;
    xTaskHandle* comms_th = input->comms_tHandle;
    const TickType_t frq = ((COMMS_COUNT_UPDATE_INTERVAL) / (portTICK_PERIOD_MS * m(s)));
    const uint8_t sys_time_update_on = SYSTEM_TIME_UPDATE_INTERVAL/COMMS_COUNT_UPDATE_INTERVAL;
    
    uint8_t sys_time_update_cnt = 0;
    vTaskDelay(((COMMS_COUNT_UPDATE_INTERVAL/m(s))*2)/portTICK_PERIOD_MS);
    TickType_t cur_tick = xTaskGetTickCount();
    while(1){
        vTaskDelayUntil(&cur_tick, frq);
        xTaskNotify(*comms_th,  __COMS_UPDATE_COUNTER, eSetValueWithoutOverwrite);
        sys_time_update_cnt++;
        if (sys_time_update_on == sys_time_update_cnt){
            sys_time_update_cnt = 0;
            xTaskNotify(*display_th,  __UPDATE_SYSTEM_TIME, eSetValueWithoutOverwrite);
        }
    }
}
    