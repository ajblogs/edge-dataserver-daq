"""
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Implements a simple HTTP/1.0 Server
"""
import os
import time
import socket
import argparse
import schedule
import threading
from collections import deque
from datetime import datetime,timedelta

# Define socket host and port
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 1531
# Default log directory
logat = ''#<defauult path to log directory>
backup_dir = ''#<default path to backup directory>

_log_actual_file_name = ''
_log_file_count = 0
_log_file_name = ''
_prev_log_file_name = ''
_log_lock =  threading.Lock()
_log_event = threading.Event()
_edge_node_restart_count = 0
def _log_increment_count():
    global _log_file_count
    _log_file_count = _log_file_count+1

def _log_maintenance(args=''):

    global _log_file_count
    global _log_file_name

    prev_log_file_name = _log_file_name
    _log_file_name = _log_actual_file_name + f'.{_log_file_count}.txt'
    f = open(_log_file_name,'w')
    f.write(f"{args}Logging data using server code from anujfalcon8@gmail.com\n")
    f.close()

    if _log_file_count > 0:
        os.system(f'mv {prev_log_file_name} {backup_dir}')

    _log_increment_count()
    print(f"{_log_file_count} is the log file count")

class _log_thread(threading.Thread):
    def __init__(self, threadId, log):
        threading.Thread.__init__(self)
        self.threadID = threadId
        self.log = log

    def run(self):
        global _log_file_name
        print ('start log backup thread')
        while True:
            # Makes sure this thread doesn't hog resources. Or need to spawn a new thread each time -- still won't guarantee a few things
            _log_event.wait()
            prev_log_file_name = _log_file_name
            # Just because you can acqrure locks doesn't mean its time to do so. The previous wait() statement will help with that
            _log_lock.acquire(blocking=True,timeout=-1)

            _log_event.clear()
            with open(prev_log_file_name,'a') as log:
                for i in range(len(self.log)):
                    log.write(self.log.pop())

            schedule.run_pending()
            print('Finished log file realted events')

            _log_lock.release()


def main():
    global _edge_node_restart_count
    global _log_file_name
    global _prev_log_file_name

    # Create socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((SERVER_HOST, SERVER_PORT))
    server_socket.listen(1)
    print('Listening on port %s ...' % SERVER_PORT)
    response = 'HTTP/1.0 200 OK\n\nHello World'
    edge_node_restart_detected = False
    _log_data = deque()
    tr1 = _log_thread(1,_log_data)
    tr1.start()
    temp_log = ''

    while True:
        # Wait for client connections
        client_connection, client_address = server_socket.accept()
        client_connection.settimeout(12)
        data_recv = ''
        try:
            while True:
                request = client_connection.recv(20000).decode()
                client_connection.sendall(response.encode())
                if not request: break
                data_recv += request
        except:
            print("Timed out. Resetting connection")

        k = data_recv.splitlines()

        # Remove unnecessary lines
        k.remove('')
        k_cleaned = []
        for i in k:
            if i[-1] == ';':
                k_cleaned.append(i)

        if len(k_cleaned) > 0:
            # ID header line with time and column info
            if k_cleaned[0][0] == '$':
                _edge_node_restart_count = _edge_node_restart_count+1
                print(f'Edge node restart detected - {_edge_node_restart_count}')

                dt = k_cleaned[0].split(';')
                header = dt[1]
                dt = dt[0]
                header = header.split('#')
                temp_log += f'{dt[1:]}\n'
                len_h = len(header)
                for i in range(len_h):
                    if i == 0:
                        temp_log += f'First column: {header[i]}\n'
                    elif i == (len_h-1):
                        temp_log += f'Last column: {header[i]}\n'
                    else:
                        temp_log += f'{i} column  : {header[i]}\n'
                temp_log += f'# seperated columns\n\n'
                del k_cleaned[0]

            # Start logging actual information
            for i in k_cleaned:
                temp_log += f"{i}\n"

            if not _log_event.is_set() and _log_lock.acquire(blocking=False):
                _log_event.set()
                _log_data.appendleft(temp_log)
                temp_log = ''
                _log_lock.release()
        client_connection.close()

    # Close socket
    server_socket.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Inputs to server.py')
    parser.add_argument('--log_dir', type=str, metavar='LOG DIRECTORY', 
                        help='Directory to store the log files')
    parser.add_argument('--backup_dir', type=str, metavar='BACKUP DIRECTORY', 
                        help='Directory to archive the log files')
    arg = parser.parse_args()
    
    if arg.log_dir:
        logat = arg.log_dir
    if arg.backup_dir:
        backup_dir = arg.backup_dir

    start_time = datetime.now().strftime("at_%H-%M-%S_on_%d-%m-%Y")
    _log_actual_file_name = f'{logat}edge_dataserver_daq_log_{start_time}'

    # Clean the log dir by backing up old files
    os.system(f'mv {logat}edge_dataserver_daq_log_* {backup_dir}')
    schedule.every().day.at("23:05:37").do(_log_maintenance)
    _log_maintenance()
    main()
