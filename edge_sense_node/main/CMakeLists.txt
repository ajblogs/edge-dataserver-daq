idf_component_register(SRCS     "sense/apds9960.c"
                                "sense/ina226.c"
                                "sense/dht22.c"
                                "actuate/st7789v.c"
                                "actuate/comms.c"
                                "others/forever_counters.c"
                                "edge_sense_node.c"
                    INCLUDE_DIRS ".")
