/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/i2c.h"

#define I2C_MASTER_TX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

/*APDS-9960 Register address map*/
#define APDS9960_SENSOR_ADDR 0x39               /*!<APDS-9960 sensor's address */
#define APDS9960_ENABLE_REG 0x80
#define APDS9960_ATIME_REG 0x81
#define APDS9960_WTIME_REG 0x82

#define APDS9960_PERS_REG 0x8C
#define APDS9960_CONFIG1_REG 0x8D
#define APDS9960_CONTROL_REG 0x8F
#define APDS9960_CONFIG2_REG 0x90
#define APDS9960_ID_REG 0x92
#define APDS9960_STATUS_REG 0x93

/*Contains the lux values*/
#define APDS9960_CDATAL_REG 0x94
#define APDS9960_CDATAH_REG 0x95
#define APDS9960_RDATAL_REG 0x96
#define APDS9960_RDATAH_REG 0x97
#define APDS9960_GDATAL_REG 0x98
#define APDS9960_GDATAH_REG 0x99
#define APDS9960_BDATAL_REG 0x9A
#define APDS9960_BDATAH_REG 0x9B

#define APDS9960_AILTL_REG 0x84
#define APDS9960_AILTH_REG 0x85
#define APDS9960_AIHTL_REG 0x86
#define APDS9960_AIHTH_REG 0x87

#define APDS9960_CICLEAR_CMD 0xE6
#define APDS9960_AICLEAR_CMD 0xE7

#define APDS9960_AGAIN_1  0b000
#define APDS9960_AGAIN_4  0b001
#define APDS9960_AGAIN_16 0b010
#define APDS9960_AGAIN_64 0b011

void initialize_APDS9960();
void apds9960_measure(void *);