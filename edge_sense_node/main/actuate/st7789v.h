/*
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "Fonts/Font16.h"
#include "Fonts/Font32rle.h"
#include "Symbols/wifi1_symbol.h"
// #include "sense/ina226.h"
// #include "Fonts/Font64rle.h"
// #include "Fonts/Font72rle.h"

static const char* ST7789V_TAG = "ST7789V";

#define PIX_WIDTH 240
#define PIX_WIDTH_START 40
#define PIX_WIDTH_END 279
#define PIX_HEIGHT 135
#define PIX_HEIGHT_START 53
#define PIX_HEIGHT_END 187
//To speed up transfers, every SPI transfer sends a bunch of lines. This define specifies how many. More means more memory use,
//but less overhead for setting up / finishing transfers. Make sure 240 is dividable by this.
#define PARALLEL_LINES 135
#define Volt true
#define Amp false
#define GRAPH_POSITIVE_VALUES_ONLY true
#define GRAPH_ALL_VALUES false
typedef int color;

/*
 The LCD needs a bunch of command/argument values to be initialized. They are stored in this struct.
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} lcd_init_cmd_t;

typedef struct {
    //Solid background color, which used as reference when color = -1
    color background;
    spi_device_handle_t spi;
} lcd_struct;


typedef struct font_struct font_struct;
struct font_struct{
    int height;
    int max_width;
    int firstchr_idx;
    const unsigned char  *widtbl;
    const unsigned char * const*  chrtbl;
    void (*decoder)(font_struct *, char, uint16_t*);
    color font_rgb24;
    color font_back_rgb24;
    // uint16_t *disp_data;
    lcd_struct *lcd;
};

typedef struct symbol_struct symbol_struct;
struct symbol_struct{
    unsigned int posx;
    unsigned int posy;
    unsigned int  height;
    unsigned int  width;
    unsigned char* symbol;
    color ones_rgb24;
    color back_rgb24;
    lcd_struct* lcd;
};

typedef struct{
 shared_struct* shared_data;
 lcd_struct* lcd;
}
display_struct;

typedef struct {
    // equal ranges for +/- if even,
    // +ve range is less by one than -ve ranges for +/- if odd,
    //Also the graph_sheet_height value
    int sheet_height;
    // int graph_sheet_height = sheet_height;
    int sheet_width;
    int plot_width;
    color plot_V_color;
    color back_V_color;
    const int V_x_offset;
    const int V_y_offset;
    color plot_A_color;
    color back_A_color;
    const int A_x_offset;
    const int A_y_offset;
    lcd_struct *lcd;

} graph_struct;

typedef struct {

    int sheet_height;// Make it odd
    int sheet_width;
    int plot_width;
    color plot_V_color;
    color plot_A_color;
    color plot_P_color;
    color Y_axis_color;
    color merge_back_color;
    color cursor_color;

    int merge_x_offset;
    int merge_y_offset;
    bool only_positive;
    lcd_struct *lcd;
} merge_graph_struct;

//Place data into DRAM. Constant data gets placed into DROM by default, which is not accessible by DMA.
DRAM_ATTR static const lcd_init_cmd_t st_init_cmds[]={
    /* Memory Data Access Control, MX=MV=1, MY=ML=MH=0, RGB=0 */

    #if SCREEN_ROTATE_180 == 1
    {0x36, {(1<<5)|(1<<6)}, 1},
    #else
    {0x36, {(1<<5)|(1<<7)}, 1},
    #endif
    /* Interface Pixel Format, 16bits/pixel for RGB/MCU interface */
    {0x3A, {0x55}, 1},
    /*Brightness setting*/
    // {0x53, {(1<<5)|(0b11<<2)}, 1}
    {0x51, {0xF0}, 1},
    /* Porch Setting */
    {0xB2, {0x0c, 0x0c, 0x00, 0x33, 0x33}, 5},
    /* Gate Control, Vgh=13.65V, Vgl=-10.43V */
    {0xB7, {0x45}, 1},
    /* VCOM Setting, VCOM=1.175V */
    {0xBB, {0x2B}, 1},
    /* LCM Control, XOR: BGR, MX, MH */
    {0xC0, {0x2C}, 1},
    /* VDV and VRH Command Enable, enable=1 */
    {0xC2, {0x01, 0xff}, 2},
    /* VRH Set, Vap=4.4+... */
    {0xC3, {0x11}, 1},
    /* VDV Set, VDV=0 */
    {0xC4, {0x20}, 1},
    /* Frame Rate Control, 60Hz, inversion=0 */
    {0xC6, {0x0f}, 1},
    /* Power Control 1, AVDD=6.8V, AVCL=-4.8V, VDDS=2.3V */
    {0xD0, {0xA4, 0xA1}, 1},
    /* Positive Voltage Gamma Control */
    {0xE0, {0xD0, 0x00, 0x05, 0x0E, 0x15, 0x0D, 0x37, 0x43, 0x47, 0x09, 0x15, 0x12, 0x16, 0x19}, 14},
    /* Negative Voltage Gamma Control */
    {0xE1, {0xD0, 0x00, 0x05, 0x0D, 0x0C, 0x06, 0x2D, 0x44, 0x40, 0x0E, 0x1C, 0x18, 0x16, 0x19}, 14},
    /* Sleep Out */
    {0x11, {0}, 0x80},
    /* Display On */
    {0x29, {0}, 0x80},
    {0, {0}, 0xff}
};

void lcd_init(spi_device_handle_t spi);
void lcd_spi_pre_transfer_callback(spi_transaction_t *t);
spi_device_handle_t spi_setup();

void lcd_cmd(spi_device_handle_t spi, const uint8_t cmd);
void lcd_data(spi_device_handle_t spi, const uint8_t *data, int len);

void send_lines(int ypos, spi_device_handle_t spi, uint16_t *linedata);
void send_rect(int xpos, int ypos, int xWidth, int yHeight, spi_device_handle_t spi, uint16_t *sqdata);
void send_finish(spi_device_handle_t spi);

color rgb24_16(color rgb);
void font16_decoder(font_struct *f, char c, uint16_t* display_data);
void font_rle_decoder(font_struct *f, char c, uint16_t* display_data);

void lcd_fill(color rgb24, lcd_struct*);
void lcd_rect(color rgb24, int xpos, int ypos, int xWidth,int yHeight, lcd_struct *);
int lcd_char(int xpos,int ypos, char c, font_struct *font);
void lcd_symbol(symbol_struct *sym);
int lcd_text(int xpos,int ypos, char *c,font_struct *font);

void display_values(float V, color Vc,float A, color Ac, font_struct *);

void display_logo(font_struct *);

void clear_display_logo(font_struct *);

void display(void *);