/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sense/dht22.h"

static const char *DHT22_TAG = "DHT22";

#define tmr_grp TIMER_GROUP_0
#define tmr TIMER_0
#define tmr_int TIMER_INTR_T0
#define TIMER_DIVIDER (80)                                     //  Hardware timer clock divider
#define TIMER_SCALE (TIMER_BASE_CLK / TIMER_DIVIDER / 1000000) // convert counter value to micro seconds

#define max_dht22_clock_data 85
static uint64_t clock_data[max_dht22_clock_data] = {0};
static uint8_t idx_data = 0;
static void IRAM_ATTR one_wire_interface_io_handle(void *args)
{
    // static uint64_t tmp = 0;
    clock_data[idx_data] = timer_group_get_counter_value_in_isr(tmr_grp, tmr);
    idx_data++;
    if (idx_data > max_dht22_clock_data)
        gpio_set_intr_type(ONE_WIRE_DATA_IO, GPIO_INTR_DISABLE);
    return;

}

void dht22_measure(void *args)
{
    initiateOneWireIO
    gpio_isr_handler_add(ONE_WIRE_DATA_IO, one_wire_interface_io_handle, NULL);
    shared_struct *input = ( shared_struct *)args;
    xTaskHandle *comms_th = input->comms_tHandle;
    xTaskHandle *display_tH = input->display_tHandle;
    float *Temp = &input->temperature;
    float *RH = &input->RH;

    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = true,
        // .clk_src = TIMER_SRC_CLK_APB,
        // .intr_type = TIMER_INTR_LEVEL,
    }; // default clock source is APB
    timer_init(tmr_grp, tmr, &config);

    // // /* Configure the alarm value and the interrupt on alarm. */
    // // /*80Mhz input clock with 80 presclar for 1uS per bit */
    // timer_isr_callback_add(tmr_grp, tmr, one_wire_interface_tmr_handle, NULL, 0);

    // gpio_install_isr_service(0);
    //hook isr handler for specific gpio pin
    timer_set_alarm_value(tmr_grp, tmr, 50000 * TIMER_SCALE);

    const TickType_t frq = (DHT22_SAMPLING_PERIOD / (portTICK_PERIOD_MS * m(s)));
    TickType_t cur_tick = xTaskGetTickCount();
    while (1)
    {
        //Control the timing of iterations
        vTaskDelayUntil(&cur_tick, frq);
        idx_data = 0;

        //Send start signal
        gpio_set_direction(ONE_WIRE_DATA_IO, GPIO_MODE_OUTPUT);
        gpio_set_level(ONE_WIRE_DATA_IO, 0);
        vTaskDelay(10 / portTICK_PERIOD_MS);

        //Start monitoring the dht22 data io for edges
        timer_set_counter_value(tmr_grp, tmr, 0);
        gpio_set_intr_type(ONE_WIRE_DATA_IO, GPIO_INTR_ANYEDGE);
        vTaskDelay(10 / portTICK_PERIOD_MS);
        timer_start(tmr_grp, tmr);

        //Drop the dht22 data io for dht22 to respond
        gpio_set_direction(ONE_WIRE_DATA_IO, GPIO_MODE_INPUT);

        //Wait for the dht22 to send all data
        vTaskDelay(10 / portTICK_PERIOD_MS);
        gpio_set_intr_type(ONE_WIRE_DATA_IO, GPIO_INTR_DISABLE);

        if (idx_data == max_dht22_clock_data)
        {
            uint64_t data = 0;
            for (int i = 4; i < 83; i += 2)
            {
                data <<= 1;
                if ((clock_data[i + 1] - clock_data[i]) > 50)
                    data |= 01;
            }
            
            if ((((data >> 32) + (data >> 24 & 0xff) + (data >> 16 & 0xff) + (data >> 8 & 0xff)) & 0xff) == (data & 0xff))
            {
                *Temp = (float)((data >> 8) & 0x0ffff) / 10.0;
                *RH = (float)((data >> 24) & 0x0ffff) / 10.0;
                ESP_LOGI(DHT22_TAG, "Temperature:%2.2f", *Temp);
                ESP_LOGI(DHT22_TAG, "Humidity:%2.2f", *RH);
                ESP_LOGI(DHT22_TAG, "Timer Group with auto reload %d no., %lld***\n", idx_data, data);
                ESP_LOGI(DHT22_TAG, "CRC passed");
                xTaskNotify(*display_tH, __MEASURED__|___MEASURED_DHT22, eSetValueWithoutOverwrite);
                xTaskNotify(*comms_th, __MEASURED__|___MEASURED_DHT22, eSetValueWithoutOverwrite);
            }
            else
                ESP_LOGE(DHT22_TAG, "CRCfailed");
        }
        else
            ESP_LOGE(DHT22_TAG, "Error in the DHT22 sample collected");
    }
}
