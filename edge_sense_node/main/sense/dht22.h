/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*https://cdn-shop.adafruit.com/datasheets/Digital+humidity+and+temperature+sensor+AM2302.pdf*/
/*default bus pulled high*/
/*M 1ms_L, M 20-40us_H, Sensor 80us_L, S 80us_H, [bit 0] S _L (START) S 26-28uS_H (0), [bit 1] S _L (START) S 70uS_H (0)*/
/*M -  master; S -  Sensor*/
/*Data every 2s*/

#pragma once
#include "system_definitions.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/timer.h"

#define initiateOneWireIO                                                       \
    gpio_config_t switch_config = {.intr_type = GPIO_INTR_DISABLE,              \
                                   .mode = GPIO_MODE_INPUT,                    \
                                   .pin_bit_mask = (1ULL << ONE_WIRE_DATA_IO),  \
                                   .pull_down_en = 0,                           \
                                   .pull_up_en = 1};                            \
    ESP_ERROR_CHECK(gpio_config(&switch_config)); 

void dht22_measure(void*);
