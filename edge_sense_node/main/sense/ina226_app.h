/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "sense/ina226.h"

void switchInitiate()
{
    gpio_config_t switch_config = {.intr_type = GPIO_INTR_DISABLE,
                                   .mode = GPIO_MODE_OUTPUT,
                                   .pin_bit_mask = (1ULL << SWITCH_IO),
                                   .pull_down_en = 1,
                                   .pull_up_en = 0};
    ESP_ERROR_CHECK(gpio_config(&switch_config));
}

#define OC_mode                   \
    gpio_set_level(SWITCH_IO, 0); \
    vTaskDelay(1 / portTICK_PERIOD_MS);

#define CC_mode                   \
    gpio_set_level(SWITCH_IO, 1); \
    vTaskDelay(1 / portTICK_PERIOD_MS);

#define CC_mode_dis OC_mode
#define OC_mode_dis CC_mode

void ina226_measure(void *input)
{
    // dummy();
    //Switch that controls CC/OC mode
    switchInitiate();
    shared_struct *ina226_input = (shared_struct *)input;
    ina226_struct m_data;
    float *OCV = &(ina226_input->ocv);
    float *CCC = &(ina226_input->ccc);
    float *CCSV = &(ina226_input->ccsv);
    xTaskHandle *display_tH = ina226_input->display_tHandle;
    xTaskHandle *comms_th = ina226_input->comms_tHandle;
    xTaskHandle *apds9960_tH = ina226_input->apds9960_tHandle;
    uint32_t notify = 0;

    const TickType_t frq = (INA226_SAMPLING_PERIOD / (portTICK_PERIOD_MS * m(s)));
    TickType_t cur_tick = xTaskGetTickCount();
    while (1)
    {

        //Control the timing of iterations
        vTaskDelayUntil(&cur_tick, frq);

        //Measure closed circuit current
        CC_mode
            vTaskDelay(10 / portTICK_PERIOD_MS);
       
        xTaskNotifyWait(0, 0, &notify, portMAX_DELAY);
        if (notify == __APDS9960_TO_INA226_I2C_FOR_200MS)
        {
        single_shot_trigger_sv_conversion();
        vTaskDelay(80 / portTICK_PERIOD_MS);
        //Chekc if getdata passed and only then update the values...

            esp_err_t err = getdata_INA226(&m_data);
            if (err == ESP_OK)
            {
                if (m_data.Icalc > 0)
                    *CCC = m_data.Icalc;
                else
                    *CCC = 0;
                *CCSV = m_data.Vshunt;
                ESP_LOGI(INA226_TAG, "I: %f, Vi: %d", m_data.Icalc,
                         m_data.raw_shunt_voltage);

                //Measure open circuit voltage
                OC_mode
                    vTaskDelay(10 / portTICK_PERIOD_MS);
                single_shot_trigger_bv_conversion();
                vTaskDelay(80 / portTICK_PERIOD_MS);
                err = getdata_INA226(&m_data);
                if (err == ESP_OK)
                {

                    if (m_data.V < 0)
                        *OCV = 0;
                    else
                        *OCV = m_data.V;
                    ESP_LOGI(INA226_TAG, "V: %f", m_data.V);

                    xTaskNotify(*display_tH, __MEASURED__ | ___MEASURED_INA226, eSetValueWithoutOverwrite);
                    xTaskNotify(*comms_th, __MEASURED__ | ___MEASURED_INA226, eSetValueWithoutOverwrite);
                }
                else
                    ESP_LOGE(INA226_TAG, "Lost measurement!");
            }
            else
                ESP_LOGE(INA226_TAG, "Lost measurement!");

            //Need to deal with lost measurements at comms
            xTaskNotify(*apds9960_tH, __INA226_TO_APDS9960_I2C_FOR_200MS, eSetValueWithoutOverwrite);
        }
    }
}
