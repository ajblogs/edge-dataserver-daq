
/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "sense/apds9960.h"
static const char *APDS9960_TAG = "APDS9960";

esp_err_t write_APDS9960(uint8_t reg_addr, uint8_t data)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (APDS9960_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data, ACK_CHECK_EN);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(APDS9960_TAG, "Error writing register (%#x)", ret);
    }
    return ret;
}

esp_err_t cmd_APDS9960(uint8_t dev_cmd)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (APDS9960_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, dev_cmd, ACK_CHECK_EN);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(APDS9960_TAG, "Error writing register (%#x)", ret);
    }
    return ret;
}


uint16_t read_APDS9960(uint8_t reg_addr, bool bytes_2)
{
    //Address must point to one of the APDS9960's register (MSB must be set)
    assert(reg_addr & 0x80);
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    uint8_t data_l = 0xff;
    uint8_t data_h = 0xff;

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (APDS9960_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(APDS9960_TAG, "Error setting the register pointer");
        return -1;
    }

    // vTaskDelay(1/portTICK_PERIOD_MS);
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (APDS9960_SENSOR_ADDR << 1) | READ_BIT, ACK_CHECK_EN);
    if (bytes_2)
        i2c_master_read_byte(cmd, &data_l, ACK_VAL);
    //NACK is needed to mark the end of read according to i2c specs. Rev. 4 - sec. 3.1.6, point 5
    //URL: http://dlnware.com/sites/dlnware.com/files/downloads/i2c-specification-version-4.0.pdf
    i2c_master_read_byte(cmd, &data_h, NACK_VAL);
    i2c_master_stop(cmd);

    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd, 1000 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        //IIC ERROR HANDLE
        ESP_LOGE(APDS9960_TAG, "Error reading register (%x)", ret);
        return -1;
    }
    i2c_cmd_link_delete(cmd);
    if (bytes_2)
        return ((data_h << 8) | data_l);
    return data_h;
}

static inline void initiate_alert_sense()
{
    gpio_config_t switch_config = {.intr_type = GPIO_INTR_DISABLE,
                                   .mode = GPIO_MODE_INPUT,
                                   .pin_bit_mask = (1ULL << APDS9960_ALERT_SENSE_IO),
                                   .pull_down_en = 0,
                                   .pull_up_en = 1};
    ESP_ERROR_CHECK(gpio_config(&switch_config));
}

static bool alert_received()
{
    if (gpio_get_level(APDS9960_ALERT_SENSE_IO) == 0)
        return true;
    return false;
}

static bool APDS9960_connected()
{
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (APDS9960_SENSOR_ADDR << 1) | WRITE_BIT,
                          ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_PORT, cmd,
                               1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK)
    {
        ESP_LOGE(APDS9960_TAG, "APDS9960 device not found!");
        return false;
    }
    return true;
}
void reset_APDS9960()
{
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_ENABLE_REG, 0));
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_ENABLE_REG, APDS9960_ENABLE_VAL));
}

static int8_t again = APDS9960_AGAIN_16;
static inline void increment_again(void)
{
    again++;
    if (again > APDS9960_AGAIN_64)
        again = APDS9960_AGAIN_64;
    else if (again < 0)
        again = 0;
}

static inline void decrement_again(void)
{
    again--;
    if (again > APDS9960_AGAIN_64)
        again = APDS9960_AGAIN_64;
    else if (again < 0)
        again = 0;
}

esp_err_t static inline update_again(void)
{
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_CONTROL_REG, ((APDS9960_CONTROL_VAL & 0xffc) | again)));

    if (read_APDS9960(APDS9960_CONTROL_REG, false) == ((APDS9960_CONTROL_VAL & 0xffc) | again))
    {
        return ESP_OK;
    }
    return ESP_FAIL;
}

void initialize_APDS9960()
{
    initiate_alert_sense();


    if(APDS9960_connected()) ESP_LOGW(APDS9960_TAG,"Connected to APDS9960");
    else ESP_LOGE(APDS9960_TAG,"APDS9960 not found");

    //Reset the device
    reset_APDS9960();
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_ATIME_REG, APDS9960_ATIME_VAL));
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_PERS_REG, APDS9960_PERS_VAL));
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_CONFIG1_REG, APDS9960_CONFIG1_VAL));
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_CONTROL_REG, ((APDS9960_CONTROL_VAL & 0xffc) | again)));
    ESP_ERROR_CHECK(write_APDS9960(APDS9960_ENABLE_REG, APDS9960_ENABLE_VAL));
    read_APDS9960(APDS9960_STATUS_REG, false);
    vTaskDelay(200 / portTICK_PERIOD_MS); 
}

#include "sense/apds9960_app.h"
