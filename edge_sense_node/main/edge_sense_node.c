/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "system_definitions.h"
#include "sense/ina226.h"
#include "sense/apds9960.h"
#include "sense/i2c_common.h"
#include "sense/dht22.h"
#include "actuate/st7789v.h"
#include "actuate/comms.h"
#include "others/forever_counters.h"
#include "others/system_time.h"

static const char *SYSTEM = "SYS";
const xTaskHandle ina226_handle;
const xTaskHandle dht22_handle;
const xTaskHandle apds9960_handle;
const xTaskHandle display_handle;
const xTaskHandle comms_handle;
const xTaskHandle st_handle;

void app_main(void)
{

  initiate_reset_detector();

  //Start and connect wifi
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  ESP_LOGI(SYSTEM, "Begin SPI");
  lcd_struct lcd = {-1, spi_setup()};
  lcd_fill(lcd.background, &lcd);

  font_struct font32 = {chr_hgt_f32, max_width_f32, firstchr_f32, widtbl_f32, chrtbl_f32, font_rle_decoder, 0x0, -1, &lcd};
  // display_restart_counter(initiate_restart_counter(),0xff0000,&font32);
  display_logo(&font32);

  ESP_LOGI(SYSTEM, "Begin I2C");
  i2c_interface_reset();
  ESP_ERROR_CHECK(i2c_master_init());
  initialize_INA226();
  initialize_APDS9960();
  //Switch that controls CC/OC mode
  // switchInitiate();

  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  shared_struct shared_data = {.ina226_tHandle = &ina226_handle,
                               .apds9960_tHandle = &apds9960_handle,
                               .display_tHandle = &display_handle,
                               .comms_tHandle = &comms_handle,
                               .dht22_tHandle = &dht22_handle};

  display_struct display_data = {.shared_data = &shared_data, .lcd = &lcd};

  xTaskCreate(display, "Display measurement", 3072,
              (void *)&display_data, configMAX_PRIORITIES-1, &display_handle);
  initiate_counters(&display_handle);

  wifi_start();

  //Requires internet connectivity
  system_time_initiate();

  clear_display_logo(&font32);
  xTaskCreate(comms, "Communication functions", 5000,
              (void *)&shared_data, configMAX_PRIORITIES-1, &comms_handle);
  xTaskCreate(dht22_measure, "Temp and RH measurement", 2048,
              (void *)&shared_data, configMAX_PRIORITIES-1, &dht22_handle);
  xTaskCreate(ina226_measure, "Circuit measurement", 2048,
              (void *)&shared_data, configMAX_PRIORITIES-1, &ina226_handle);
  xTaskCreate(apds9960_measure, "Measure irradiance", 2048,
              (void *)&shared_data, configMAX_PRIORITIES-1, &apds9960_handle);
  xTaskCreate(reset_counters, "Reset counters", 2048, 
              (void *)&shared_data, configMAX_PRIORITIES-2, &reset_handle);
  xTaskCreate(system_time, "System time and trigger management", 2048, 
              (void *)&shared_data, configMAX_PRIORITIES, &st_handle);

  //To avoid deadlock (Maybe timeout is a better solution)
  // xTaskNotify(ina226_handle, __APDS9960_TO_INA226_I2C_FOR_200MS, eSetValueWithoutOverwrite);
  // while(1){
  //     ESP_LOGI(SYSTEM,"OCV: %f and CCC: %f",shared_data.ocv, shared_data.ccc);
  //     vTaskDelay(2000/portTICK_PERIOD_MS);
  // }
  // dht22_measure_app();
  while (1)
  {
    // dht22_measure_app();
    vTaskDelay(portMAX_DELAY);
  }
}
