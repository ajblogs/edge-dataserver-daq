/* 
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "others/forever_counters.h"

static const char *RRC_TAG = "RRC";
#define ESP_INTR_FLAG_DEFAULT 0

static uint64_t buffer_loss_count_fch = 0;

#define RESTART_COUNTER_KEY "rst_cnt"
#define SERVER_DOWN_COUNTER_KEY "svr_down_cnt"
#define SERVER_SUCCESSFULLY_SENT_COUNTER_KEY "bufr_loss_cnt"
#define WIFI_CONNECT_COUNTER_KEY "wifi_dc_cnt"

static const xTaskHandle *display_th;

static void IRAM_ATTR reset_counters_isr_handler(void *arg)
{
    xTaskNotifyFromISR(reset_handle, 1, eSetValueWithOverwrite, pdFALSE);
}

void initiate_counters(xTaskHandle *dth)
{
    display_th = dth;
    nvs_handle_t nvs_handle;
    ESP_ERROR_CHECK(nvs_open("critical", NVS_READWRITE, &nvs_handle));

    //Update the restart counter
    int32_t counter = 0;
    esp_err_t err = nvs_get_i32(nvs_handle, RESTART_COUNTER_KEY, &counter);
    switch (err)
    {
    case ESP_OK:
        break;
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(RRC_TAG, "The value is not initialized yet!\n");
        break;
    default:
        ESP_LOGE(RRC_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
    }
    counter++;
    ESP_LOGW(RRC_TAG, "Restart count: %d", counter);
    ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, RESTART_COUNTER_KEY, counter));
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___RESTART_COUNT_ID | (uint16_t)counter, eSetValueWithoutOverwrite);
    vTaskDelay(10 / portTICK_PERIOD_MS);

    counter = 0;
    err = nvs_get_i32(nvs_handle, SERVER_DOWN_COUNTER_KEY, &counter);
    switch (err)
    {
    case ESP_OK:
        break;
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(RRC_TAG, "The value is not initialized yet!\n");
        ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, SERVER_DOWN_COUNTER_KEY, counter));
        break;
    default:
        ESP_LOGE(RRC_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
    }
    ESP_LOGW(RRC_TAG, "Server down count: %d", counter);
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___SERVER_DOWN_COUNT_ID | (uint16_t)counter, eSetValueWithoutOverwrite);
    vTaskDelay(10 / portTICK_PERIOD_MS);

    counter = 0;
    err = nvs_get_i32(nvs_handle, WIFI_CONNECT_COUNTER_KEY, &counter);
    switch (err)
    {
    case ESP_OK:
        break;
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(RRC_TAG, "The value is not initialized yet!\n");
        ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, WIFI_CONNECT_COUNTER_KEY, counter));
        break;
    default:
        ESP_LOGE(RRC_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
    }
    ESP_LOGW(RRC_TAG, "Wifi connected count: %d", counter);
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___WIFI_CONNECT_COUNT_ID | (uint16_t)counter, eSetValueWithoutOverwrite);

    ESP_ERROR_CHECK(nvs_commit(nvs_handle));
    nvs_close(nvs_handle);
}

void initiate_reset_detector()
{
    gpio_config_t switch_config = {.intr_type = GPIO_INTR_NEGEDGE,
                                   .mode = GPIO_MODE_INPUT,
                                   .pin_bit_mask = (1ULL << RESET_COUNTER_PIN),
                                   .pull_down_en = 0,
                                   .pull_up_en = 1};
    ESP_ERROR_CHECK(gpio_config(&switch_config));
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    gpio_isr_handler_add(RESET_COUNTER_PIN, reset_counters_isr_handler, NULL);
}

void reset_counters(void *inp)
{
    shared_struct *input = (shared_struct *)inp;
    xTaskHandle *display_th = input->display_tHandle;
    uint32_t notify;
    int32_t restart_counter;
    while (1)
    {
        xTaskNotifyWait(0, 0, &notify, portMAX_DELAY);
        if (notify == 1)
        {
            ESP_LOGW(RRC_TAG, "Key press detected!");
            vTaskDelay(2000 / portTICK_PERIOD_MS);
            if (gpio_get_level(RESET_COUNTER_PIN) == 0)
            {
                nvs_handle_t nvs_handle;
                ESP_ERROR_CHECK(nvs_open("critical", NVS_READWRITE, &nvs_handle));
                ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, RESTART_COUNTER_KEY, 0));
                ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, SERVER_DOWN_COUNTER_KEY, 0));
                ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, WIFI_CONNECT_COUNTER_KEY, 0));
                ESP_ERROR_CHECK(nvs_commit(nvs_handle));
                ESP_LOGW(RRC_TAG, "All counters reset to 0");
                nvs_close(nvs_handle);
                xTaskNotify(*display_th, __RESET_COUNTERS, eSetValueWithoutOverwrite);
            }
        }
    }
}

void increment_server_down_counter()
{
    nvs_handle_t nvs_handle;
    ESP_ERROR_CHECK(nvs_open("critical", NVS_READWRITE, &nvs_handle));

    //Update the server down counter
    int32_t counter = 0;
    ESP_ERROR_CHECK(nvs_get_i32(nvs_handle, SERVER_DOWN_COUNTER_KEY, &counter));
    counter++;
    ESP_LOGW(RRC_TAG, "Server down counter: %d", counter);
    ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, SERVER_DOWN_COUNTER_KEY, counter));
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___SERVER_DOWN_COUNT_ID | (uint16_t)counter, eSetValueWithoutOverwrite);
    ESP_ERROR_CHECK(nvs_commit(nvs_handle));
    nvs_close(nvs_handle);
}

void increment_buffer_loss_counter()
{
    buffer_loss_count_fch++;
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___BUFFER_LOSS_COUNT_ID | (uint16_t)buffer_loss_count_fch, eSetValueWithoutOverwrite);
}

void increment_wifi_connect_counter()
{
    nvs_handle_t nvs_handle;
    ESP_ERROR_CHECK(nvs_open("critical", NVS_READWRITE, &nvs_handle));

    //Update the server down counter
    int32_t counter = 0;
    ESP_ERROR_CHECK(nvs_get_i32(nvs_handle, WIFI_CONNECT_COUNTER_KEY, &counter));
    counter++;
    ESP_LOGW(RRC_TAG, "WIfi connected counter: %d", counter);
    ESP_ERROR_CHECK(nvs_set_i32(nvs_handle, WIFI_CONNECT_COUNTER_KEY, counter));
    xTaskNotify(*display_th, __COUNTER_UPDATE | ___WIFI_CONNECT_COUNT_ID | (uint16_t)counter, eSetValueWithoutOverwrite);
    ESP_ERROR_CHECK(nvs_commit(nvs_handle));
    nvs_close(nvs_handle);
}