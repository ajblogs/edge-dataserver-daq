/*
The MIT License (MIT)

Copyright (c) 2021 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "actuate/st7789v.h"

#define X_PIX_STATIC 130 // Value based on observation
#define Y_PIX_STATIC 28  // Value based on observation

static uint16_t server_down_count_ = 0;
static uint16_t buffer_loss_count_ = 0;
static uint16_t wifi_connect_count_ = 0;
static uint16_t restart_count_ = 0;
static uint16_t server_successfully_sent_count_ = 0;

/*Function to display Voltage,Current and Power*/
void display_logo(font_struct *font)
{
    char *container = "Anuj Falcon !";
    lcd_text(25, Y_PIX_STATIC + 30, container, font);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
}
void clear_display_logo(font_struct *font)
{
    lcd_rect(font->lcd->background, 25, Y_PIX_STATIC + 30, 200, 32, font->lcd);
}
// /*Function to display Voltage,Current and Power*/
void display_values(float V, color Vc, float A, color Ac, font_struct *font)
{
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;

    char container[11] = {};
    sprintf(container, "%06.3f V", V);
    font->font_rgb24 = Vc;
    int w = lcd_text(7, 2, container, font);

    sprintf(container, "%06.5f A", A);
    font->font_rgb24 = Ac;
    lcd_text(w + 16, 2, container, font);
}

void display_ird(float ird, color RHc, font_struct *font)
{
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;

    char container[15] = {};
    if (ird >= 1000)
    {
        ird /= 1000.0;
        sprintf(container, "%04.1fiR   ", ird);
    }
    else
        sprintf(container, "%04.1fir   ", ird);
    font->font_rgb24 = RHc;
    lcd_text(160, 36, container, font);
}

void display_temp_rh(float RH, color RHc, float T, color Tc, font_struct *font)
{
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;

    char container[11] = {};
    sprintf(container, "%04.1f%%", RH);
    font->font_rgb24 = RHc;
    int w = lcd_text(7, 36, container, font);

    sprintf(container, "%04.1fC", T);
    font->font_rgb24 = Tc;
    lcd_text(w + 8, 36, container, font);
}

void display_capture_send_progress(uint32_t wifi_status, uint16_t buffer_cnt, font_struct *font)
{
    if (buffer_cnt <= Mcnt_THRESHOLD && wifi_status == __WIFI_CONNECTED)
    {
        int w = lcd_char(102, 70, ' ', font);
        uint8_t divs = 8;
        uint8_t progress = buffer_cnt / (Mcnt_THRESHOLD / divs);

        uint8_t progress1 = progress + 1;
        uint8_t progress2 = progress + 2;
        if (progress1 > (divs - 1))
        {
            progress1 = 0;
            progress2 = 1;
        }
        else if (progress2 > (divs - 1))
        {
            progress2 = 0;
        }
        for (uint8_t i = (divs - 6); i < divs; i++)
        {


            if (i == progress2)
                font->font_rgb24 = 0xE5C8C8;
            else if (i == progress1)
                font->font_rgb24 = 0xE5AAAA;
            else if (i == progress)
                font->font_rgb24 = error_c;
            else
                font->font_rgb24 = disable_c;
            w = lcd_char(w, 70, '>', font);
        }
        lcd_char(w, 70, ' ', font);
    }
    else if (wifi_status == __WIFI_CONNECTED && buffer_cnt < BUFFER_SAMPLES)
    {
        char container[11] = {};
        font->font_rgb24 = error_c;
        sprintf(container, " >>>>>> ");
        lcd_text(102, 70, container, font);
    }
    else
    {
        char container[11] = {};
        font->font_rgb24 = disable_c;
        sprintf(container, " ---");
        int w = lcd_text(100, 70, container, font);
        font->font_rgb24 = error_c;
        w = lcd_char(w, 70, 'x', font);
        font->font_rgb24 = disable_c;
        sprintf(container, "---");
        w = lcd_text(w, 70, container, font);
    }
}

static uint8_t dws_temp = 0;
void display_wifi_info(uint32_t wifi_status, symbol_struct *w_sym, esp_ip4_addr_t *dev_ip, font_struct *font)
{
    if (wifi_status == __WIFI_CONNECTED)
        w_sym->ones_rgb24 = ok_c;
    else if (wifi_status == __WIFI_DISCONNECTED_FOR_LONG)
        w_sym->ones_rgb24 = disable_c;
    else
    {
        if (dws_temp == 0)
        {
            dws_temp = 1;
            if (wifi_status == __WIFI_DISCONNECTED)
                w_sym->ones_rgb24 = error_c;
            else if (wifi_status == __WIFI_CONNECTED_DATA_NOT_SENT)
                w_sym->ones_rgb24 = warning_c;
            else
                w_sym->ones_rgb24 = 0;
        }
        else
        {
            dws_temp = 0;
            w_sym->ones_rgb24 = w_sym->lcd->background;
        }
    }
    lcd_symbol(w_sym);

    char container[45] = {};
    sprintf(container, IPSTR, IP2STR(dev_ip));
    int w = 0;
    if (wifi_status == __WIFI_CONNECTED || wifi_status == __WIFI_CONNECTED_DATA_NOT_SENT)
    {
        font->font_rgb24 = normal_c;
        w = lcd_text(7, 70, container, font);
    }
    else
    {
        font->font_rgb24 = disable_c;
        w = lcd_text(7, 70, container, font);
    }

    w = 150;
    sprintf(container, "SERVER:" COMMS_DATA_SERVER_IP_PORT);
    if (wifi_status == __WIFI_CONNECTED_DATA_NOT_SENT)
    {
        font->font_rgb24 = error_c;
        sprintf(container, "SERVER: CHK ");
        lcd_text(w, 70, container, font);
    }
    else if (wifi_status == __WIFI_CONNECTED)
    {
        font->font_rgb24 = normal_c;
        lcd_text(w, 70, container, font);
    }
    else
    {
        font->font_rgb24 = disable_c;
        lcd_text(w, 70, container, font);
    }
}

void display_time(color tc, font_struct *font)
{
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;
    char container[30] = {};
    time_t now = 0;
    struct tm timeinfo = {0};
    setenv("TZ", "UTC-2", 1);
    tzset();
    time(&now);
    localtime_r(&now, &timeinfo);
    strftime(container, sizeof(container), "%c ", &timeinfo);

    font->font_rgb24 = tc;
    lcd_text(60, 115, container, font);
}

static uint8_t dmai_temp = 0;
void display_measurement_associated_info(uint32_t wifi_status, uint16_t buffer_cnt, uint64_t sn, font_struct *font)
{
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;
    char container[20] = {};
    sprintf(container, "S:%08llx", sn);
    font->font_rgb24 = watch_c;
    int w = lcd_text(60, 95, container, font);
    sprintf(container, " B:%04x ", buffer_cnt);

    if (buffer_cnt <= Mcnt_THRESHOLD)
        font->font_rgb24 = watch_c;
    else if (buffer_cnt < BUFFER_SAMPLES)
        font->font_rgb24 = error_c;
    else
    {
        dmai_temp++;
        if (dmai_temp == 1)
        {
            font->font_rgb24 = error_c;
        }
        else if (dmai_temp == 3)
        {
            font->font_rgb24 = error_c;
            sprintf(container, " B:LOSS ");
        }
        else if (dmai_temp == 2 || dmai_temp == 4)
        {
            font->font_rgb24 = font->lcd->background;
            if (dmai_temp == 4)
                dmai_temp = 0;
        }
    }
    w = lcd_text(w, 95, container, font);
}

void display_counter(color cc, font_struct *font)
{
    static uint8_t counter = DELAY_DISPLAY_COUNTER - 1;
    counter++;

    //Only first 8 bit is possible to display for the restart counter
    if (font->font_back_rgb24 == -1)
        font->font_back_rgb24 = font->lcd->background;
    font->font_rgb24 = cc;
    static char container[17] = {};
    switch (counter)
    {
    case DELAY_DISPLAY_COUNTER:
        sprintf(container, "RST:%02x   ", restart_count_);
        break;
    case (2 * DELAY_DISPLAY_COUNTER):
        sprintf(container, "WIC:%02x   ", wifi_connect_count_);
        break;
    case (3 * DELAY_DISPLAY_COUNTER):
        sprintf(container, "SRD:%02x   ", server_down_count_);
        counter = 0;
        break;
    default:
        // counter = 0;
        break;
    }
    lcd_text(190, 95, container, font);
}

void display(void *input)
{
    display_struct *display_input = (display_struct *)input;
    float *OCV = &display_input->shared_data->ocv;
    float *CCC = &display_input->shared_data->ccc;
    float *Temp = &display_input->shared_data->temperature;
    float *RH = &display_input->shared_data->RH;
    float *IRD = &display_input->shared_data->ird;
    // xTaskHandle *ina226_tH = display_input->shared_data->ina226_tHandle;
    lcd_struct *lcd = display_input->lcd;
    font_struct font16 = {chr_hgt_f16, max_width_f16, firstchr_f16, widtbl_f16, chrtbl_f16, font16_decoder, 0x0, -1, lcd};
    font_struct font32 = {chr_hgt_f32, max_width_f32, firstchr_f32, widtbl_f32, chrtbl_f32, font_rle_decoder, 0x0, -1, lcd};
    symbol_struct wifi_sym = {5, PIX_HEIGHT - wifi1_height - 3, wifi1_height, wifi1_width, wifi1_symbol, ok_c, -1, lcd};
    uint16_t *unsent_data_ = &display_input->shared_data->unsent_data;
    esp_ip4_addr_t *dev_ip = &display_input->shared_data->ip;
    uint64_t *sn_ = &display_input->shared_data->sn;
    uint32_t notify = 0;
    uint32_t notify_class = 0;
    while (1)
    {
        //Control the timing of iterations
        xTaskNotifyWait(0, 0, &notify, portMAX_DELAY);
        notify_class = (notify & __MESSAGE_CLASS__);
        // notify_super_class = (notify & __MESSAGE_SUPER_CLASS__);
        // notify_sub_class = (notify & ___MESSAGE_SUBCLASS___);

        // ESP_LOGW(ST7789V_TAG, "Notification: %#04x", notify);
        if (notify_class == __MEASURED__)
        {
            uint32_t notify_sub_class = (notify & ___MESSAGE_SUBCLASS___);
            static uint8_t i = 0;
            static float ocv = 0;
            static float ccc = 0;
            switch (notify_sub_class)
            {
            case ___MEASURED_INA226:

#if ACCUUMULATE_X_INA226_MEASUREMENTS_B4_DISPLAY > 1
                if (i != ACCUUMULATE_X_INA226_MEASUREMENTS_B4_DISPLAY)
                {
                    ocv += *OCV;
                    ccc += *CCC;
                }
                else
                {
                    ocv /= (float)ACCUUMULATE_X_INA226_MEASUREMENTS_B4_DISPLAY;
                    ccc /= (float)ACCUUMULATE_X_INA226_MEASUREMENTS_B4_DISPLAY;
                    display_values(ocv, 0xff0000, ccc, 0xDD9300, &font32);
                    i = 0;
                }
                i++;
#else
                display_values(*OCV, 0xff0000, *CCC, 0xDD9300, &font32);
#endif

                break;
            case ___MEASURED_DHT22:
                ESP_LOGI(ST7789V_TAG, "Temperature:%2.2f *C",*Temp);
                ESP_LOGI(ST7789V_TAG, "Humidity:%2.2f %%", *RH);
                display_temp_rh(*RH, 0xff0000, *Temp, 0xDD9300, &font32);
                break;
            case ___MEASURED_APDS9960:
                // ESP_LOGI(ST7789V_TAG, "Irradiance: %f uW/cm2",*IRD);
                display_ird(*IRD, 0xff0000, &font32);
                break;
            default:
                ESP_LOGE(ST7789V_TAG, "Shouldn't happen while decoding counter info");
                while (1)
                    ;
                break;
            }
        }
        else if ((notify & __MESSAGE_SUPER_CLASS__) == __WIFI_MESSAGE__)
        {
            display_wifi_info(notify_class, &wifi_sym, dev_ip, &font16);
            display_measurement_associated_info(notify_class, *unsent_data_, *sn_, &font16);
            display_capture_send_progress(notify_class, *unsent_data_, &font16);
        }

        else if (notify_class == __RESET_COUNTERS)
        {
            server_down_count_ = 0;
            buffer_loss_count_ = 0;
            wifi_connect_count_ = 0;
            restart_count_ = 0;
        }
        else if (notify_class == __COUNTER_UPDATE)
        {
            uint16_t notify_data = (uint16_t)(notify & ____MESSAGE_IS____);
            uint32_t notify_sub_class = (notify & ___MESSAGE_SUBCLASS___);
            switch (notify_sub_class)
            {
            case ___RESTART_COUNT_ID:
                restart_count_ = notify_data;
                break;
            case ___WIFI_CONNECT_COUNT_ID:
                wifi_connect_count_ = notify_data;
                break;
            case ___SERVER_DOWN_COUNT_ID:
                server_down_count_ = notify_data;
                break;
            case ___BUFFER_LOSS_COUNT_ID:
                buffer_loss_count_ = notify_data;
                break;
            case ___SERVER_SUCCESSFULLY_SENT_COUNT_ID:
                server_successfully_sent_count_ = notify_data;
                break;
            default:
                ESP_LOGE(ST7789V_TAG, "Shouldn't happen while decoding counter info");
                while (1)
                    ;
                break;
            }
        }
        else if (notify_class == __UPDATE_SYSTEM_TIME)
        {
            display_time(normal_c, &font16);
            display_counter(error_c, &font16);
        }
    }
}
